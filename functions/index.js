const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

exports.logTestComplete = functions.testLab
    .testMatrix()
    .onComplete(testMatrix => {
        const {
            testMatrixId,
            createTime,
            state,
            outcomeSummary
        } = testMatrix;

        console.log(
            `TEST ${testMatrixId} (created at ${createTime}): ${state}. ${outcomeSummary ||
        ''}`
        );
    });

exports.liveUrlChange = functions.database.ref('Jukkrid/value').onWrite((event) => {
    // Exit if data is deleted.
    if (!change.after.exists()) return null;

    // Grab the current value of what was written to the Realtime Database.
    const value = change.after.val();
    console.log('The liveurl value is now', value);

    // Build the messaging notification, sending to the 'all' topic.
    var message = {
        notification: {
            title: 'Database change',
            body: 'The liveurl value has changed to ' + value
        },
        topic: 'liveurl_changed'
    };

    // Send the message.
    return admin.messaging().send(message)
        .then((message) => {
            console.log('Successfully sent message:', message);
        })
        .catch((error) => {
            console.error('Error sending message:', error);
        });
});

exports.fcm_singleDevice = functions.database.ref('{machineID}/value').onWrite(async (change, context) => {

    const machineID = context.params.machineID;
    const getDeviceTokensPromise = admin.database().ref(`/${machineID}/token/token`).once('value');

    console.log("the Machine id : "+machineID);
    const results = await Promise.all([getDeviceTokensPromise]);
    tokensSnapshot = results[0];

    tokens = tokensSnapshot.val();
    console.log("log of token : "+tokens);
    // Exit if data is deleted.
    if (!change.after.exists()) return null;

    // Grab the current value of what was written to the Realtime Database.
    const payload = {
        notification: {
          title: 'You have a new Medical Checkup',
          body: `${machineID} is now following you.`,
          icon: "myicon"
        }
      };

      return admin.messaging().sendToDevice(tokens,payload);
});